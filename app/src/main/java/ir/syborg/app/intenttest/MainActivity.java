package ir.syborg.app.intenttest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.LinkedHashMap;
import java.util.Set;

public class MainActivity extends AppCompatActivity {


  Button btnCat;
  Button btnDog;
  Button btnDuck;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    btnCat = (Button) findViewById(R.id.btnCat);
    btnDog = (Button) findViewById(R.id.btnDog);
    btnDuck = (Button) findViewById(R.id.btnDuck);

    LinkedHashMap<String , String> color=SampleColorData.get();
    Set<String> keySet=color.keySet();
    for(String key:keySet){
      String value=color.get(key);
     Log.i("TAG" , key + " " + " #"+value);
    }

    View.OnClickListener listener = new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        String title = view.getTag().toString().trim();
          Intent intent = new Intent(MainActivity.this, SecondActivity.class);
        intent.putExtra("title" , title);
        startActivity(intent);

      }

    };

    btnCat.setOnClickListener(listener);
    btnDog.setOnClickListener(listener);
    btnDuck.setOnClickListener(listener);

  }
}
